package mock

import (
	"time"

	"bitbucket.org/henspe/voi/validator"
)

const (
	twoDaysInSec    = 172800
	apiTickDuration = time.Second * 1
)

// Vehicle specify the last known information about a vehicle type
type Vehicle struct {
	State           validator.State
	LastStateChange float64
}

// ChangeState sets the state of the pointer receiver to that of the provided argument,
// on the condition that the state transition is valid for that specific type of role
func (v *Vehicle) ChangeState(currState, newState validator.State, r validator.Role) error {
	v.State = newState
	v.LastStateChange = 0.0
	return nil
}

// Run keeps track of how long ago the vehicle was last updated
func (v *Vehicle) Run() {
	t := time.NewTicker(apiTickDuration)
	for {
		select {
		// new tick by timer
		case <-t.C:
			v.LastStateChange += apiTickDuration.Seconds()

			if v.State == validator.Ready && v.LastStateChange > twoDaysInSec {
				if err := validator.ChangeStateIfPossible(v.State, validator.Unknown, validator.Admin, v); err != nil {
					// log error
				}
			}
		}
	}
}

// NewVehicle instance
func NewVehicle() *Vehicle {
	return &Vehicle{}
}
