package mock

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"
)

const (
	tickDuration             = time.Second * 1
	decReadyChargePerSec     = 0.01
	decRidingChargePerSec    = 0.1
	incCollectedChargePerSec = 0.1
)

type centralAPI interface {
	CallAPI(string, string, string) error
}

// LocalVehicle specify information about a mocked physical vehicle
// It has a hook to a mocked server API
type LocalVehicle struct {
	centralAPI
	State  string
	Charge float32
	Settings
}

// Settings contain vehicle configuration provided upon creation
type Settings struct {
	batteryLowLevel float32
	afterTimeCharge time.Time
}

type newState struct {
	State string `json:"state"`
	Role  string `json:"role"`
}

// Run the vehicle locally
// The CallAPI can be interpreted as API calls in order to check if the state can be changed
func (v *LocalVehicle) Run() {
	t := time.NewTicker(tickDuration)
	for {
		select {
		case <-t.C:
			fmt.Printf("New tick! State: %s, Charge %f\n", v.State, v.Charge)

			// Some mock implementation in order for the battery level to increase/decrease
			// depending on the current state of the scooter
			if v.State == "Ready" && v.Charge > 0.0 {
				v.Charge -= float32(tickDuration.Seconds() * decReadyChargePerSec)
			} else if v.State == "Riding" && v.Charge > 0.0 {
				v.Charge -= float32(tickDuration.Seconds() * decRidingChargePerSec)
			} else if v.State == "Collected" && v.Charge < 1.0 {
				v.Charge += float32(tickDuration.Seconds() * incCollectedChargePerSec)
			}

			if v.State == "Riding" && v.Charge < v.batteryLowLevel {
				err := v.CallAPI(v.State, "BatteryLow", "Admin")
				if err != nil {
					// log error locally (logged server side), UNABLE TO CHANGE STATE
					return
				}
				v.State = "BatteryLow"
			}

			if v.State == "BatteryLow" {
				err := v.CallAPI(v.State, "Bounty", "Admin")
				if err != nil {
					// log error locally (logged server side), UNABLE TO CHANGE STATE
					return
				}
				v.State = "Bounty"
			}

			late, err := v.isTimeLate(time.Now())
			if err != nil {
				// log error locally (logged server side), INTERNAL ERROR
				return
			}

			if v.State == "Ready" && late {
				err := v.CallAPI(v.State, "Bounty", "Admin")
				if err != nil {
					// log error locally (logged server side), UNABLE TO CHANGE STATE
					return
				}
				v.State = "Bounty"
			}
		}
	}
}

// LocalAPI sets up and starts a REAL API on port 8080 and listens for POST
// requests on /scooter
func (v *LocalVehicle) LocalAPI() {
	http.HandleFunc("/scooter", func(w http.ResponseWriter, r *http.Request) {
		var ns newState
		decoder := json.NewDecoder(r.Body)
		if err := decoder.Decode(&ns); err != nil {
			http.Error(w, "Invalid JSON", http.StatusBadRequest)
			return
		}

		fmt.Printf("Calling API with, old_state: %s, new_state: %s, role: %s\n", v.State, ns.State, ns.Role)

		err := v.CallAPI(v.State, ns.State, ns.Role)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		v.State = ns.State

		w.WriteHeader(http.StatusOK)
		w.Write([]byte("State changed successfully\n"))
	})

	log.Fatal(http.ListenAndServe(":8080", nil))
}

// isTimeLate compares the current time (passed as an argument) to the time set as a threshold for when
// the vehicle is automatically set to the "bounty" state
func (v *LocalVehicle) isTimeLate(t time.Time) (bool, error) {
	lT, err := time.Parse("3:04PM", t.Format("3:04PM"))

	if err != nil {
		return false, fmt.Errorf("internal error")
	}

	return lT.After(v.afterTimeCharge), nil
}

// NewLocalVehicle sets up a mock vehicle with specified low-battery level and time-threshold for when the vehicle is supposed
// to be set to certain states
func NewLocalVehicle(api centralAPI, state string, charge float32, lowBattery float32, lateTime string) (*LocalVehicle, error) {
	tf, err := time.Parse("3:04PM", lateTime)
	if err != nil {
		return &LocalVehicle{}, err
	}

	return &LocalVehicle{
		centralAPI: api,
		State:      state,
		Charge:     charge,
		Settings: Settings{
			batteryLowLevel: lowBattery,
			afterTimeCharge: tf,
		},
	}, nil
}
