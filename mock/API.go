package mock

import "bitbucket.org/henspe/voi/validator"

type logger interface {
	LogFailedTransition(error)
	LogSuccessfulTransition(string, string, string)
}

// API holds the server-representation of a localvehicle
type API struct {
	Vehicle *Vehicle // example vehicle, vehicles would normally be stored in a DB
	logger
}

// CallAPI calls the validation package and updates an abstract vehicle stored in a DB, for example
// The vehicle state is updated, logged and the time since last update is reset to 0
// If a vehicle fails to update itself for 48h its state (in DB) is automatically set to "Unknown"
func (a API) CallAPI(currState, newState, role string) error {
	a.Vehicle.LastStateChange = 0.0
	err := validator.ChangeStateIfPossible(
		validator.GetState(currState),
		validator.GetState(newState),
		validator.GetRole(role),
		a.Vehicle,
	)

	// Log the outcome
	if err != nil {
		a.LogFailedTransition(err)
	} else {
		a.LogSuccessfulTransition(currState, newState, role)
	}

	// return err to the "localvehicle" mock
	return err
}

// NewAPI sets up the mock API. It takes a vehicle and a logger as argument.
// The vehicle is a server-sie representation of a vehicle and is used to update
// information about the time since last successful state update
func NewAPI(v *Vehicle, l logger) API {
	return API{Vehicle: v, logger: l}
}
