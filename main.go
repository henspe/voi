package main

import (
	"bitbucket.org/henspe/voi/logger"
	"bitbucket.org/henspe/voi/mock"
)

func main() {
	// Create a new mock instance of a vehicle as represented on the server
	// It will run in a seperate go-routine and implements the automatic state
	// update if the last state update was > some threshold
	testVehicle := mock.NewVehicle()
	go testVehicle.Run()

	// Create a new logger which is passed to the mock API in order to log state
	// transition requests. The mock API can be called upon by the mock representation
	// of the local vehicle (interpreted as the physical vehicle)
	logger := logger.New()
	testAPI := mock.NewAPI(testVehicle, logger)

	// Create a new representation of a local vehicle which is the holder of state in this solution
	// It consumes the mock API, a starting state, a battery level, a low battery level and a time
	// on the 12:00PM form which marks the late night charge threshold for the automatic state transition.
	// It is run in a go-routine and can be accessed through an exposed API
	startState := "Ready"
	startCharge := float32(1.0)
	lowCharge := float32(0.2)
	lateTime := "9:30PM"
	localTestVehicle, err := mock.NewLocalVehicle(testAPI, startState, startCharge, lowCharge, lateTime)
	if err != nil {
		// log error
		return
	}
	go localTestVehicle.Run()
	localTestVehicle.LocalAPI() // start API
}
