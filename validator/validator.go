package validator

import "fmt"

// StateHandler changes the state of the provided object;
// its arguments are: (current state, new state, role of one performing the transition)
type StateHandler interface {
	ChangeState(State, State, Role) error
}

// Role defines available user roles
type Role int

// State defines available vehicle states
type State int

// Role constants
const (
	User Role = iota + 1
	Hunter
	Admin
)

// State constants
const (
	Ready State = iota + 1
	Riding
	BatteryLow
	Bounty
	Collected
	Dropped
	ServiceMode
	Terminated
	Unknown
)

// String returns the name of the Role
func (r Role) String() string {
	var rs string
	switch r {
	case User:
		rs = "User"
	case Hunter:
		rs = "Hunter"
	case Admin:
		rs = "Admin"
	}
	return rs
}

// String returns the name of the State
func (s State) String() string {
	var rs string
	switch s {
	case Ready:
		rs = "Ready"
	case Riding:
		rs = "Riding"
	case BatteryLow:
		rs = "BatteryLow"
	case Bounty:
		rs = "Bounty"
	case Collected:
		rs = "Collected"
	case Dropped:
		rs = "Dropped"
	case ServiceMode:
		rs = "ServiceMode"
	case Terminated:
		rs = "Terminated"
	case Unknown:
		rs = "Unknown"
	}
	return rs
}

// GetRole returns the Role constant given its name
func GetRole(s string) Role {
	var rs Role
	switch s {
	case "User":
		rs = User
	case "Hunter":
		rs = Hunter
	case "Admin":
		rs = Admin
	}
	return rs
}

// GetState returns the State constant given its name
func GetState(s string) State {
	var rs State
	switch s {
	case "Ready":
		rs = Ready
	case "Riding":
		rs = Riding
	case "BatteryLow":
		rs = BatteryLow
	case "Bounty":
		rs = Bounty
	case "Collected":
		rs = Collected
	case "Dropped":
		rs = Dropped
	case "ServiceMode":
		rs = ServiceMode
	case "Terminated":
		rs = Terminated
	case "Unknown":
		rs = Unknown
	}
	return rs
}

// ChangeStateIfPossible validates the provided states to ensure that the transition from
// currState to newState by a user of role r is valid. If the transition is valid it runs the
// ChangeState function which changes the state of the provided vehicle
func ChangeStateIfPossible(currState, newState State, r Role, s StateHandler) error {
	if err := isTransitionPossible(currState, newState, r); err != nil {
		return err
	}
	return s.ChangeState(currState, newState, r)
}

func isTransitionPossible(currState, newState State, r Role) error {
	// admins can do anything
	if r == Admin {
		return nil
	}

	if currState == newState {
		return fmt.Errorf("provided state is already set")
	}

	possibleTransitions, ok := transitionTree[r]

	if !ok {
		return fmt.Errorf("no transitions available for provided role (%s)", r.String())
	}

	nextState, ok := possibleTransitions[currState]

	if !ok {
		return fmt.Errorf("no transitions available from current state (%v) for provided role (%v)", currState.String(), r.String())
	}

	if nextState != newState {
		return fmt.Errorf("can't transition to %v from %v", newState.String(), currState.String())
	}

	return nil
}
