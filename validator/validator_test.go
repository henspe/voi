package validator

import (
	"testing"
)

type defaultStateKeeper struct {
	State
}

func (d *defaultStateKeeper) ChangeState(c, n State, r Role) error {
	d.State = n
	return nil
}

// Test the main function of the package responsible for validating provided states.
// If the transition is valid the ChangeState function of defaultStateKeeper is run which
// should replace the state
func TestChangeStateIfPossible(t *testing.T) {
	startState := Ready
	nextState := Riding
	role := User
	d := &defaultStateKeeper{State: startState}

	err := ChangeStateIfPossible(startState, nextState, role, d)
	if err != nil {
		t.Errorf("expected err==nil, was %v", err)
	}

	if d.State != nextState {
		t.Errorf("expected d.State==%v, was %v", nextState, d.State)
	}
}

// Test valid and invalid state transitions as defined in transitions.go
func TestIsTransitionPossible(t *testing.T) {
	type testState struct {
		currentState State
		newState     State
		ok           bool
	}

	testUserTransitions := []testState{
		testState{Riding, Ready, true},
		testState{Ready, Riding, true},
		testState{Bounty, Ready, false},
	}

	testHunterTransitions := []testState{
		testState{Bounty, Collected, true},
		testState{Collected, Dropped, true},
		testState{Dropped, Collected, false},
		testState{Ready, Collected, false},
	}

	for _, ts := range testUserTransitions {
		if err := isTransitionPossible(ts.currentState, ts.newState, User); (err == nil) != ts.ok {
			t.Errorf("expected %v, got %v with (%v->%v, %v)", ts.ok, err, ts.currentState.String(), ts.newState.String(), "User")
		}
	}

	for _, ts := range testHunterTransitions {
		if err := isTransitionPossible(ts.currentState, ts.newState, Hunter); (err == nil) != ts.ok {
			t.Errorf("expected %v, got %v with (%s->%s, %s)", ts.ok, err, ts.currentState.String(), ts.newState.String(), "Hunter")
		}
	}
}

// Testing static functions for getting and stringify package constants
var roleConsts = map[Role]string{
	User:   "User",
	Hunter: "Hunter",
	Admin:  "Admin",
}

var stateConsts = map[State]string{
	Ready:       "Ready",
	Riding:      "Riding",
	BatteryLow:  "BatteryLow",
	Bounty:      "Bounty",
	Collected:   "Collected",
	Dropped:     "Dropped",
	ServiceMode: "ServiceMode",
	Terminated:  "Terminated",
	Unknown:     "Unknown",
}

func TestString(t *testing.T) {
	for k, v := range stateConsts {
		if c := k.String(); c != v {
			t.Errorf("expected (%v) == %s, got %s", k, v, c)
		}
	}

	for k, v := range roleConsts {
		if c := k.String(); c != v {
			t.Errorf("expected (%v) == %s, got %s", k, v, c)
		}
	}
}

func TestGetRole(t *testing.T) {
	for k, v := range roleConsts {
		if c := GetRole(v); c != k {
			t.Errorf("expected (%s) == %v, got %v", v, k, c)
		}
	}
}
func TestGetState(t *testing.T) {
	for k, v := range stateConsts {
		if c := GetState(v); c != k {
			t.Errorf("expected (%s) == %v, got %v", v, k, c)
		}
	}
}
