package validator

// This file contains definitions of valid state transitions for different
// user roles

type roleTransitions map[State]State
type stateTransitions map[Role]roleTransitions

var userTransitions = roleTransitions{
	Ready:  Riding,
	Riding: Ready,
}

var hunterTransitions = roleTransitions{
	Bounty:    Collected,
	Collected: Dropped,
	Dropped:   Ready,
}

var transitionTree = stateTransitions{
	User:   userTransitions,
	Hunter: hunterTransitions,
}
