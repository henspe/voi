State Transitions

# Information

The /validator package is the solution to the problem. It contains all valid state transitions which can be taken by a certain user role.
It does not handle the automatic state transitions, as that is handled by the /mock/localvehicle.go and /mock/vehicle.go. 

<br>

/mock/vehicle.go is thought to be a server representation of the abstract vehicle which could be stored in a database. It handles the automatic
state transition from Ready->Unknown if the last known state update was > some given time (48h). This could for example be checked from time to time 
and be stored in a database.

<br>

/mock/localvehicle is thought to be a physical representation of the abstract vehicle. It stores and updates its state according to the response 
from the mock API which runs the validator package in order to ensure that the provided transition is valid. It implements the automatic state transition
rules, such as Riding->LowBattery if its battery < some value (20%). 
It can be manipulated by user inputs (which simulates a user requesting to, for example claim the vehicle and use it). Perform POST requests with
JSON-data on the form {"state":"Ready", "role":"User"} on localhost:8080/scooter to test it out.

# Requirements

* If the transition IS NOT valid, return err
* If the transition IS valid, return nil

* The solution SHOULD include the git history
* The solution MUST be stateless. Any required state will be provided

# Roles

* Regular end-users
* Hunters, responsible of charging the scooters
* Admins, who can do anything

# States
Operational modes

- Ready, operational and ready to be claimed by anyone
- Battery_low, low battery but operational. Can ONLY be claimed by a hunter
- Bounty, only available for hunters to be charged
- Riding, an end-user is currently using the vehicle and it may not be claimed by anyone
- Collected, has been picked up by a hunter to be charged
- Dropped, returned by a hunter after being charged

Non-operational modes

- Service_mode
- Terminated
- Unknown

# State changes
Specific rules

* After 9:30 pm local time: If Ready -> Bounty
* If battery < 20% and state is about to change from Riding to Ready, change to Battery_low instead
* After 48h since last state change: Ready -> Unknown

Normal changes

* Ready -> (all) Riding
* Riding -> (all) Ready

* Battery_low -> (automatic) Bounty
* Bounty -> (hunter) Collected

* Collected -> (hunter) Dropped
* Dropped -> (hunter) Ready

Special

* Any state -> (admin) Any state 


