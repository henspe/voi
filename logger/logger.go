package logger

import (
	"fmt"
	"time"
)

type Logger struct {
}

func (l Logger) LogFailedTransition(err error) {
	t := time.Now()
	fmt.Printf("\x1b[31;1m [Error] \x1b[34;1m[%s]\x1b[0m \x1b[36;1m[%s]\x1b[0m\x1b[0m %s\n", t.Format("2006-01-02 15:04:05.0000"), "placeholder_scooter_id", err.Error())
}

func (l Logger) LogSuccessfulTransition(cs, ns, r string) {
	msg := fmt.Sprintf("State changed from %s to %s by a %s", cs, ns, r)
	t := time.Now()
	fmt.Printf("\x1b[32;3m [Success] \x1b[34;1m[%s]\x1b[0m \x1b[36;1m[%s]\x1b[0m\x1b[0m %s\n", t.Format("2006-01-02 15:04:05.0000"), "placeholder_scooter_id", msg)
}

func New() Logger {
	return Logger{}
}
